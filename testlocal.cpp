#include <CL/cl2.hpp>
#include <fstream>
#include <sstream>
#include <string>
#include <iostream>
#include <math.h>
#include <stdexcept>
#include <opencv2/opencv.hpp>

cl::Program createProgram(std::vector<std::string> filenames){
	cl::Program::Sources sources;

	for(int i=0; i<filenames.size(); i++){
		std::ifstream ifs(filenames[i]);
		std::string kernel_code;
		std::ostringstream ss;
		ss << ifs.rdbuf();
		kernel_code = ss.str();

		sources.push_back({kernel_code.c_str(), kernel_code.length()});
	}
	
	std::vector<cl::Platform> platforms;
	cl::Platform::get(&platforms);
	cl::Platform platform = platforms[0];
	std::vector<cl::Device> devices;
	platform.getDevices(CL_DEVICE_TYPE_ALL, &devices);
	cl::Device device = devices[0];

	cl::Context context({device});
	cl::Program program(context, sources);
	if(program.build({device}) != CL_SUCCESS){
		std::cout<<"Error building: "<<program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device)<<std::endl;
	}
	return program;
}


cv::Mat getImage(){
	cv::Mat img = cv::imread("../frame.tiff", CV_LOAD_IMAGE_GRAYSCALE);
	cv::Mat resized;
	cv::resize(img, resized, cv::Size(), 0.25, 0.25, CV_INTER_CUBIC);
	cv::imshow("Input", resized);
	// resized.convertTo(resized, CV_32F);
	img.convertTo(resized, CV_32F);
	// cv::Mat appended_rows = cv::Mat::ones(2, resized.cols, CV_32F);
	// resized.push_back(appended_rows);
	return resized;
}

int main(){
	cv::Mat im = getImage();

	cl::Program program = createProgram({"../conv_vertical.cl", "../conv_horizontal.cl"});
	cl::Context context = program.getInfo<CL_PROGRAM_CONTEXT>();
	cl::vector<cl::Device> devices = context.getInfo<CL_CONTEXT_DEVICES>();
	cl::Device device = devices.front();

	std::vector<float> in (im.begin<float>(), im.end<float>());
	
	cl_int err;
	std::vector<float> out(in.size(), 0);
	std::vector<float> filter(17, 0);
	for (int i=0; i<filter.size(); i++){
		filter[i] = 1.0/filter.size();
	}
	cl::Buffer in_buffer(context, CL_MEM_READ_ONLY | CL_MEM_HOST_NO_ACCESS | CL_MEM_COPY_HOST_PTR, in.size()*sizeof(float), in.data());
	cl::Buffer out_buffer(context, CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY, sizeof(float)*in.size());
	cl::Buffer in_out_buffer(context, CL_MEM_READ_WRITE, sizeof(float)*in.size());
	cl::Buffer filter_buffer(context, CL_MEM_READ_ONLY | CL_MEM_HOST_NO_ACCESS | CL_MEM_COPY_HOST_PTR, filter.size()*sizeof(float), filter.data());

	cl::Kernel kernel_ver(program, "conv_vertical");
	kernel_ver.setArg(0, in_buffer);
	kernel_ver.setArg(1, in_out_buffer);
	kernel_ver.setArg(2, filter_buffer);
	kernel_ver.setArg(3, sizeof(float)*(16*(16+2*(filter.size()/2))), nullptr);

	cl::Kernel kernel_hor(program, "conv_horizontal");
	kernel_hor.setArg(0, in_out_buffer);
	kernel_hor.setArg(1, out_buffer);
	kernel_hor.setArg(2, filter_buffer);
	kernel_hor.setArg(3, sizeof(float)*(16*(16+2*(filter.size()/2))), nullptr);

	cl::CommandQueue queue(context, device);
	cl::Event ev_hor, ev_ver;
	auto start = std::chrono::high_resolution_clock::now();
	queue.enqueueNDRangeKernel(kernel_ver, cl::NullRange, cl::NDRange(im.rows, im.cols), cl::NDRange(16, 16), NULL, &ev_hor);
	cl::vector<cl::Event> ev_hor_vec{ev_hor};
	queue.enqueueNDRangeKernel(kernel_hor, cl::NullRange, cl::NDRange(im.rows, im.cols), cl::NDRange(16, 16), &ev_hor_vec, &ev_ver);
	queue.enqueueReadBuffer(out_buffer, CL_TRUE, 0, sizeof(float)*out.size(), out.data());
	cl::finish();
	auto elapsed = std::chrono::high_resolution_clock::now() - start;
	long long microseconds = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();
	std::cout<<"OpenCL:\t"<<microseconds<<std::endl;

	cv::Mat output_img(im.rows, im.cols, CV_32F, out.data());
	output_img.convertTo(output_img, CV_8U);
	cv::resize(output_img, output_img, cv::Size(), 0.25, 0.25, CV_INTER_CUBIC);
	cv::imshow("OpenCl", output_img);

	cv::waitKey(0);
	return 0;
}