#define HALF 8

__kernel void conv_vertical( const __global float* in,
						__global float* out,
						__constant float* filter,
						__local float* cache){
	int i = get_global_id(0);
	int j = get_global_id(1);
	int h = get_global_size(0);
	int w = get_global_size(1);
	int il = get_local_id(0);
	int jl = get_local_id(1);
	int hl = get_local_size(0);
	int wl = get_local_size(1);

	cache[ jl + (il + HALF) * wl ] = in[ j + i * w ];

	if(i < HALF || i >= h - HALF){
		barrier(CLK_LOCAL_MEM_FENCE);
		return;
	}

	if(il < HALF){
		cache[ jl + il * wl ] = in[ j + (i - HALF) * w ];
	}
	else if(il >= hl - HALF){
		cache[ jl + (il + 2*HALF) * wl ] = in[ j + (i + HALF) * w ];
	}
	barrier(CLK_LOCAL_MEM_FENCE);

	float sum = 0.0;
	float FILTER_SIZE = 2*HALF+1;
	for(int k = 0; k < FILTER_SIZE; k++){
		sum += cache[ jl + (il + k) * wl] * filter[k];
	}
	out [ j + i * w ] = sum;
}
