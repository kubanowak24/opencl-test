#include <CL/cl2.hpp>
#include <fstream>
#include <sstream>
#include <string>
#include <iostream>
#include <math.h>
#include <stdexcept>

cl::Program createProgram(std::string filename){
	cl::Program::Sources sources;

	std::ifstream ifs(filename);
	std::string kernel_code;
	std::ostringstream ss;
	ss << ifs.rdbuf();
	kernel_code = ss.str();

	sources.push_back({kernel_code.c_str(), kernel_code.length()});

	std::vector<cl::Platform> platforms;
	cl::Platform::get(&platforms);
	cl::Platform platform = platforms[0];
	std::vector<cl::Device> devices;
	platform.getDevices(CL_DEVICE_TYPE_ALL, &devices);
	cl::Device device = devices[0];

	cl::Context context({device});
	cl::Program program(context, sources);
	if(program.build({device}) != CL_SUCCESS){
		std::cout<<"Error building: "<<program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device)<<std::endl;
	}
	return program;
}


int main(){
	cl::Program program = createProgram("../reduction.cl");
	cl::Context context = program.getInfo<CL_PROGRAM_CONTEXT>();
	cl::vector<cl::Device> devices = context.getInfo<CL_CONTEXT_DEVICES>();
	cl::Device device = devices.front();

	std::vector<int> in(256);
	int sum = 0;
	for(int i=0; i<in.size(); i++){
		in[i] = i;
		sum += i;
	}

	cl::Kernel kernel(program, "reduction");
	cl_int err;
	auto workGroupSize = kernel.getWorkGroupInfo<CL_KERNEL_WORK_GROUP_SIZE>(device, &err);
	auto numWorkGroups = in.size() / workGroupSize;

	cl::Buffer in_buffer(context, CL_MEM_READ_ONLY | CL_MEM_HOST_NO_ACCESS | CL_MEM_COPY_HOST_PTR, in.size()*sizeof(int), in.data());
	cl::Buffer out_buffer(context, CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY, sizeof(int)*numWorkGroups);

	kernel.setArg(0, in_buffer);
	kernel.setArg(1, sizeof(int)*workGroupSize, nullptr);
	kernel.setArg(2, out_buffer);

	std::vector<int> out(numWorkGroups);

	cl::CommandQueue queue(context, device);
	queue.enqueueNDRangeKernel(kernel, cl::NullRange,
		cl::NDRange(in.size()), cl::NDRange(workGroupSize));
	// global dimensions must be a multiple of local dimensions
	queue.enqueueReadBuffer(out_buffer, CL_TRUE, 0, sizeof(int)*out.size(), out.data());
	cl::finish();
	std::cout<<"out buffer size "<<out.size()<<std::endl;
	for(int i=0; i<out.size(); i++){
		std::cout<<"output buffer value "<<out[i]<<std::endl;
	}

	return 0;
}