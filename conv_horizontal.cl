#define HALF 8

__kernel void conv_horizontal( const __global float* in,
						__global float* out,
						__constant float* filter,
						__local float* cache){
	int i = get_global_id(0);
	int j = get_global_id(1);
	int w = get_global_size(1);
	int il = get_local_id(0);
	int jl = get_local_id(1);
	int wl = get_local_size(1);

	cache[ jl + HALF + il * (wl + 2*HALF) ] = in[ j + i * w ];

	if(j < HALF || j >= w - HALF){
		barrier(CLK_LOCAL_MEM_FENCE);
		return;
	}

	if(jl < HALF){
		cache[ jl + il * (wl + 2*HALF) ] = in[ j - HALF + i * w ];
	}
	else if(jl >= wl - HALF){
		cache[ jl + 2*HALF + il * (wl + 2*HALF) ] = in[ j + HALF + i * w ];
	}
	barrier(CLK_LOCAL_MEM_FENCE);

	float sum = 0.0;
	float FILTER_SIZE = 2*HALF+1;
	for(int k = 0; k < FILTER_SIZE; k++){
		sum += cache[ jl + il * (wl+2*HALF) + k] * filter[k];
	}
	out [ j + i * w ] = sum;
}
