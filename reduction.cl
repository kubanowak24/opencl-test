#pragma OPENCL EXTENSION cl_intel_printf: enable

__kernel void reduction(__global int* inData, __local int* localData, __global int* outData){
	int globalId = get_global_id(0);
	int localSize = get_local_size(0);
	int localId = get_local_id(0);

	localData[localId] = inData[globalId];
	barrier(CLK_LOCAL_MEM_FENCE);

	for(int i = localSize >> 1; i > 0; i >>= 1){
		if(localId < i){
			localData[localId] += localData[localId + i];
		}
		barrier(CLK_LOCAL_MEM_FENCE);
	}

	if(localId == 0){
		outData[get_group_id(0)] = localData[localId];
	}
}