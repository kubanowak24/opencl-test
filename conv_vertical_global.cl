#define HALF 8

__kernel void conv_vertical_global( const __global float* in,
						__global float* out,
						__constant float* filter){
	int i = get_global_id(0);
	int j = get_global_id(1);
	int h = get_global_size(0);
	int w = get_global_size(1);

	if(i < HALF || j < HALF || i >= h - HALF || j >= w - HALF){
		return;
	}

	float sum = 0.0;
	float FILTER_SIZE = 2*HALF+1;
	for(int k = 0; k < FILTER_SIZE; k++){
		sum += in [ j + (i+k-HALF) * w ] * filter[k];
	}
	out [ j + i * w ] = sum;
}